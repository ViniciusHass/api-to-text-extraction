flask-restx==0.4.0
Flask-SQLAlchemy==2.1
Flask-Cors==3.0.7
Werkzeug==0.16.0
loguru==0.4.1
kafka-python==2.0.2
numpy