from setuptools import setup, find_packages

setup(
    name='api',
    version='1.0.0',
    description='API to receve documents and extract text',
    url='-',
    author='Vinicius Hass',

    classifiers=[
        'Development Status :: Developer/Alpha',
        'Programming Language :: Python :: 3.9',
    ],

    keywords='',

    packages=find_packages(),

    install_requires=['flask-restplus==0.9.2',
                      'flask-restx==0.4.0',
                      'Flask-Cors==3.0.7',
                      'Werkzeug==0.16.0',
                      'loguru==0.4.1',
                      'kafka-python==2.0.2',
                      'numpy',
                      'Flask-SQLAlchemy==2.1'],
)
