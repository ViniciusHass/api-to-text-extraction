from flask import request
from loguru import logger


class ControllLog:

    def __init__(self):
        print('ControllLog class for log controll')

    @staticmethod
    def debug(messages):
        """
            Information of interest for developers fix the code.
        """
        logger.debug("[+] - {} --- {}".format(request.remote_addr, messages))

    @staticmethod
    def info(messages):
        """
            Information of interest of suport team to try
            discover context of a error
        """
        logger.info("[+] - {} --- {}".format(request.remote_addr, messages))

    @staticmethod
    def success(messages):
        logger.success("[-] - {} --- {}".format(request.remote_addr, messages))

    @staticmethod
    def warning(messages):
        """
            statements that describe potentially harmful events or states in the program.
        """
        logger.warning("[-] - {} --- {}".format(request.remote_addr, messages))

    @staticmethod
    def error(messages):
        """
            statements that describe non-fatal errors in the application; this level is
            used quite often to log handled exceptions.
        """
        logger.error("[-] - {} --- {}".format(request.remote_addr, messages))

    @staticmethod
    def critical(messages):
        """
            statements representing the most serious error conditions,
            supposedly resulting in the termination of the program .
        """
        logger.critical("[-] - {} --- {}".format(request.remote_addr, messages))


# Objeto de log
objLogger = ControllLog()

