import hashlib

def hasher(string_value):
    hashed_string = hashlib.sha256(string_value.encode('utf-8')).hexdigest()
    return hashed_string