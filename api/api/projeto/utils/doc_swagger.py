from flask_restx import fields
from projeto.rest import api

OUTPUT_DATA = api.model('exemplo_swagger_output',
                        {'messages': fields.String(required=True,
                                                  description='Sucess or Error mensage'),
                         'status': fields.String(required=True,
                                                 description='Status code'),
                         'data': fields.Raw(required=False,
                                            description="Data response location")})
