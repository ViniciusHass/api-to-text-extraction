import logging

from flask_restx import Api
import settings
from projeto.constants import CodeHttp, Message
from projeto.utils.Logger import objLogger
from projeto.utils.Response import objResponse

log = logging.getLogger(__name__)


api = Api(version='1.0', title='Documentation',
          description='Documentation of the swagger')


@api.errorhandler
def default_error_handler(e):
    objLogger.error(Message.ERROR_NOT_TREATMENT)

    if not settings.FLASK_DEBUG:
        return objResponse.send_exception(NotTreatmentException, Message.ERROR_NOT_TREATMENT, CodeHttp.ERROR_500)
