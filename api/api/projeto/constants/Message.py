ERROR_GENERIC = 'Generic message error'
ERROR_NOT_TREATMENT = 'Error not trated'
ERROR_NOT_FOUND = 'Not Found'
ERROR_NONE = 'Unknow Error'
ERROR_EXCEPTION = 'Exception Error'

REQUEST = "Request Successful"
# Error Business #
ERROR_BO = 'Exception: business rule error and something unexpected occurred.'
ERROR_DIVISION = 'Exception: error div for zero.'

# Debug Business #
DEBUG_B64_DECODE = "Debug: Deconding base64."
DEBUG_B64_ENCODE = "Debug: Enconding base64."

# Sucess Business #
SUCESS_EXEMPLO = "Success: test message."  # Example
