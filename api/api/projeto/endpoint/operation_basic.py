import logging

import settings
from flask import request
from flask_restx import Resource
from projeto.rest import api
from projeto.constants import CodeHttp, Message
from projeto.utils import doc_swagger
from projeto.rest import objLogger, objResponse

log = logging.getLogger(__name__)
ns = api.namespace(settings.ENDPOINTS, description='Post operation.')


@ns.route(settings.ROUTE)
class PostsCollection(Resource):

    def get(self):
        """
        Method GET
        arguments: key - <hash256>
        """

        return "test"

    @api.response(200, 'Send with sucess.')
    @api.marshal_with(doc_swagger.OUTPUT_DATA)
    def post(self):
        """
        Method POST
        arguments: file - <base64> 
        """
        objLogger.debug(Message.REQUEST)
        request_data = request.get_json()

        try:
            file = request_data["file"]
            # TODO call business rule (bo)

        except KeyError as error:
            response = objResponse.send_exception(objError=error, messages=Message.ERROR_BO, status=CodeHttp.ERROR_500)
            objLogger.error(messages=Message.ERROR_BO)
	
        except TypeError as error:
            response = objResponse.send_exception(objError=error, messages=Message.ERROR_BO, status=CodeHttp.ERROR_500)
            objLogger.error(messages=Message.ERROR_BO)

        else:
            response = objResponse.send_success(messages=Message.SUCESS_EXEMPLO, status=CodeHttp.SUCCESS_200, data=teste)
            objLogger.success(messages=Message.SUCESS_EXEMPLO)

        return response
